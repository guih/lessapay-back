'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TransferSchema = new Schema({
  type: String,
  value: Number,
  form: { type: String, ref: 'User'},
  to: { type: String, ref: 'User' },
  meta: { type: Object, default: {}},
  timex: {type: String, default: new Date().toISOString()},
  status: {
    type: [{
      type: String,
      enum: ['sent', 'waiting', 'scheduled']
    }],
    default: ['sent']
  }
});

module.exports = mongoose.model('Transfer', TransferSchema);