'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CollectorSchema = new Schema({
  title: {
    type: String,
    required: 'Insira um tipo para a mãquina'
  },
  registered: {
    type: Date,
    default: Date.now
  },
  id: String,
  logs: Object,
  meta: Object,
  status: {
    type: [{
      type: String,
      enum: ['offline', 'online', 'unkown']
    }],
    default: ['offline']
  }
});

module.exports = mongoose.model('Collector', CollectorSchema);