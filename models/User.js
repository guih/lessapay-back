'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new Schema({
  fullname: {
    type: String,
    required: 'Insira um nome'
  },
  uname: {
    type: String,
    required: 'Insira um nome'
  },
  registered: {
    type: Date,
    default: Date.now
  },
  email: String,
  cpf: String,
  rg: String,
  password: String,
  phone: String,
  dob: String,
  balance: {type: Number, default: 0.0},
  pushId: String,
  smsCode: String,
  status: {
    type: [{
      type: String,
      enum: ['banned', 'active', 'suspend']
    }],
    default: ['active']
  },
  role: {
    type: [{
      type: String,
      enum: ['adm', 'moderator', 'user']
    }],
    default: ['user']
  }
});

module.exports = mongoose.model('User', UserSchema);