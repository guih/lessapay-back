'use strict';

const crypto = require('crypto');
const ENC_KEY = crypto.createHash('sha256').update(String('b98ef67c73c1b03b47154')).digest('base64').substr(0, 32);

const IV = "5183666c72eec9e4"; 



exports.encrypt = function(val) {
  let cipher = crypto.createCipheriv('aes-256-cbc', ENC_KEY, IV);
  let encrypted = cipher.update(val, 'utf8', 'base64');
  encrypted += cipher.final('base64');
  return encrypted;
};

exports.decrypt = function (encrypted) {
  let decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
  let decrypted = decipher.update(encrypted, 'base64', 'utf8');
  return (decrypted + decipher.final('utf8'));
};


