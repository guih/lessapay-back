 var request = require('request');
 var cheerio = require('cheerio');
 var readline = require('readline');
 var fs = require('fs');



// return;
 var cookieJar = request.jar();


function getLoginForm(fn){
 	request({
	 	method: 'GET',
	 	jar: cookieJar,
	 	uri: 'http://www.send2u.com.br/sms/CadastroCliente.xhtml'
	}, function(err, res, body){
		console.log('login get')
	 	if( res ){
	 
	 		var $ = cheerio.load(body);
	 		var loginForm = $('#FormCadCliente').serializeArray();
	 		var loginFormObject = {};
			loginForm.forEach(function(value, key){
				loginFormObject[value.name] = value.value;
			});
	 		fn(null, loginFormObject['javax.faces.ViewState'], { headers: res.headers, jar: cookieJar });
	        return;

	 	}

	 	fn(err);	 	
	});
}


function registerUser(args, fn){

	var headers = {
	    'Pragma': 'no-cache',
	    'Origin': 'http://www.send2u.com.br',
	    'Accept-Encoding': 'gzip, deflate',
	    'Accept-Language': 'en-US,en;q=0.9',
	    'Faces-Request': 'partial/ajax',
	    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
	    'Accept': 'application/xml, text/xml, */*; q=0.01',
	    'Cache-Control': 'no-cache',
	    'X-Requested-With': 'XMLHttpRequest',
	    'Connection': 'keep-alive',
	};

	var dataString = 'javax.faces.partial.ajax=true&javax.faces.source=btnSalvarCliente&javax.faces.partial.execute=%40all&btnSalvarCliente=btnSalvarCliente&FormCadCliente=FormCadCliente&javax.faces.ViewState='+args.ViewState+'&txtTipo_input=FISICA&txtTipo_focus=&txtCPFCNPJ='+args.cpf+'&txtCel='+Math.random().toFixed(11).replace('0.', '')+'&txtNome='+args.email.name+'&txtEmail='+args.email.str;

	var headers = Object.assign(args.headers, headers);


	var options = {
	    url: 'http://www.send2u.com.br/sms/CadastroCliente.xhtml',
	    method: 'POST',
	    headers: args.headers,
	    body: dataString,
	    jar: args.jar
	};

	function callback(error, response, body) {
	    if (!error && response.statusCode == 200) {
	        fn(args.jar, response.headers['set-cookie']);
	    }
	}

	request(options, callback);

}

function generateEmail(fn){
	var headers = {
	    'accept': 'application/json'
	};

	var options = {
	    url: 'https://www.developermail.com/api/v1/mailbox',
	    method: 'PUT',
	    headers: headers
	};

	function callback(error, response, body) {
		console.log('email get')

		console.log(response.statusCode )
	    if (!error && response.statusCode == 200) {
	        fn(null, JSON.parse(body));
	        return;
	    }

	    fn(error);
	}

	request(options, callback);	
}



function generateCPF(fn){
	function gera_random(n) {
	    var ranNum = Math.round(Math.random()*n);
	    return ranNum;
	}
	function mod(dividendo,divisor) {
		return Math.round(dividendo - (Math.floor(dividendo/divisor)*divisor));
	}
	function cpf() {
		var n = 9;
		var n1 = gera_random(n);
	 	var n2 = gera_random(n);
	 	var n3 = gera_random(n);
	 	var n4 = gera_random(n);
	 	var n5 = gera_random(n);
	 	var n6 = gera_random(n);
	 	var n7 = gera_random(n);
	 	var n8 = gera_random(n);
	 	var n9 = gera_random(n);
	 	var d1 = n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
	 	d1 = 11 - (mod(d1,11));
	 	if (d1>=10) d1 = 0;
	 	var d2 = d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
	 	d2 = 11 - (mod(d2,11));
	 	if (d2>=10) d2 = 0;

		console.log('cpf get')

	 	return ''+n1+n2+n3+'.'+n4+n5+n6+'.'+n7+n8+n9+'-'+d1+d2;

	}

	fn(cpf());
}


function checkIds(args, fn){

	var headers = {
	    'accept': 'application/json',
	    'X-MailboxToken': args.token
	};

	var options = {
	    url: 'https://www.developermail.com/api/v1/mailbox/'+args.name,
	    headers: headers
	};

	function callback(error, response, body) {
	    if (!error && response.statusCode == 200) {
	    	var result = JSON.parse(body);

	    	if( result.result.length == 0 ){
	    		checkIds(args, fn);
	    	}else{
	       		fn(null,result);

	    	}
	        return;
	    }else{
	    	fn(error)
	    }
	}

	request(options, callback);

}

function getMessage(args, fn){

	var headers = {
	    'accept': 'application/json',
	    'X-MailboxToken': args.token,
	    'Content-Type': 'application/json'
	};

	var dataString = JSON.stringify( args.id );

	var options = {
	    url: 'https://www.developermail.com/api/v1/mailbox/'+args.name+'/messages',
	    method: 'POST',
	    headers: headers,
	    body: '['+dataString+']'
	};

	console.log(options, '<<<<<<<<<<')


	function callback(error, response, body) {
	    if (!error && response.statusCode == 200) {
	        fn(null, JSON.parse(body));
	        return;
	    }else{
	    	fn(error)
	    }
	}

	request(options, callback);

}


function login(cod, user, fn){

	var request = require('request');

	var newJar = request.jar();



	var headers = {
	    'Connection': 'keep-alive',
	    'Pragma': 'no-cache',
	    'Cache-Control': 'no-cache',
	    'Upgrade-Insecure-Requests': '1',
	    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
	    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
	    'Accept-Encoding': 'gzip, deflate',
	    'Accept-Language': 'en-US,en;q=0.9',
	    'Origin': 'http://www.send2u.com.br',
'Content-Type': 'application/x-www-form-urlencoded',

'Host': 'www.send2u.com.br',
'Origin': 'http://www.send2u.com.br',
'Pragma': 'no-cache',
'Referer': 'http://www.send2u.com.br/sms/index.xhtml',

	};

	var options = {
	    url: 'http://www.send2u.com.br/sms/index.xhtml',
	    headers: headers,
	    jar: newJar
	};

	function callback(error, response, body) {
	    if (!error && response.statusCode == 200) {
	    	var $ = cheerio.load(body);
	    	var ViewState = $('[name="javax.faces.ViewState"]').val();
	   
	    	var cook = response.headers['set-cookie'][0].match(/([A-Z0-9]+)/gi)[1];

	  
	    	headers['Cookie'] = 'JSESSIONID='+cook;


			var dataString = 'FormLogin=FormLogin&javax.faces.ViewState='+ViewState+'&FormLogin%3AbtnLogin=&FormLogin%3Aj_idt13='+cod+'&FormLogin%3Aj_idt17='+user+'&FormLogin%3Aj_idt20='+user;

			console.log(dataString)
			var options = {
			    url: 'http://www.send2u.com.br/sms/login.xhtml',
			    method: 'POST',
			    headers: headers,
			    body: dataString,
			    jar: newJar
			};

			function callback(error, response, body) {

			    if (!error && response.statusCode == 200) {


				var options = {
				    url: 'http://www.send2u.com.br/sms/CadastroEventoLivre.xhtml',
				    headers: headers
				};

				function callback(error, response, body) {
				    if (!error && response.statusCode == 200) {
				    	var $ = cheerio.load(body);
				    	var vs = ($('#javax.faces.ViewState').val())
			        	fn(newJar, headers, vs);

				        // console.log(body);
				    }
				}

				request(options, callback);

   					// var cook = response.headers['set-cookie'][0].match(/([A-Z0-9]+)/gi)[1];	  
	    			// headers['Cookie'] = 'JSESSIONID='+cook;
				// console.log(body, response.headers)

			    }

			}

			request(options, callback);        
	    }
	}

	request(options, callback);

}

function send(number, text, jar, head, VS, fn){

var request = require('request');

var headers = {
    'Connection': 'keep-alive',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
    'Referer': 'http://www.send2u.com.br/sms/index.xhtml',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'en-US,en;q=0.9',
};

var options = {
    url: 'http://www.send2u.com.br/sms/CadastroEventoLivre.xhtml',
    headers: headers,
    jar: jar
};

function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
        // console.log(body);
        var querystring = require('querystring');

        var $ = cheerio.load( body );

        var data = querystring.parse($('[id="FormCadEvento"]').serialize());

		var headers = {
		    'Pragma': 'no-cache',
		    'Origin': 'http://www.send2u.com.br',
		    'Accept-Encoding': 'gzip, deflate',
		    'Accept-Language': 'en-US,en;q=0.9',
		    'Faces-Request': 'partial/ajax',
		    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
		    'Accept': 'application/xml, text/xml, */*; q=0.01',
		    'Cache-Control': 'no-cache',
		    'X-Requested-With': 'XMLHttpRequest',
		    'Connection': 'keep-alive',
		    'Referer': 'http://www.send2u.com.br/sms/CadastroEventoLivre.xhtml',
		    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
		};

		var dataString = 'javax.faces.partial.ajax=true&javax.faces.source=btnSalvarEnviarEvento&javax.faces.partial.execute=%40all&btnSalvarEnviarEvento=btnSalvarEnviarEvento&FormCadEvento=FormCadEvento&javax.faces.ViewState='+data['javax.faces.ViewState']+'&j_idt122=0&j_idt124=&txtNome=&autoContatos_input=&autoContatos_hinput=&txtCel=33998160257&j_idt149=&txtInicio_input=&autoTemplate_input=&autoTemplate_hinput=&j_idt171=Envio+de+Teste+da+Send2u+SMS.+Texto+fixo+para+testes.+Entrar+em+contato%3A+11+98532-1425.';

		var options = {
		    url: 'http://www.send2u.com.br/sms/CadastroEventoLivre.xhtml',
		    method: 'POST',
		    headers: headers,
		    body: dataString,
		    jar: jar
		};

		function callback(error, response, body) {
		    if (!error && response.statusCode == 200) {
		        var ft = body.match(/<update id="javax\.faces\.ViewState"><!\[CDATA\[(.*)\]\]><\/update>/g);

		        if( ft[0] ){
		        	var piece = ft[0].match(/<!\[CDATA\[(.*)]]>/g);

		        	var vs = piece[0].replace("<![CDATA[", "").replace("]]>", "");



					var headers = {
					    'Pragma': 'no-cache',
					    'Origin': 'http://www.send2u.com.br',
					    'Accept-Encoding': 'gzip, deflate',
					    'Accept-Language': 'en-US,en;q=0.9',
					    'Faces-Request': 'partial/ajax',
					    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
					    'Accept': 'application/xml, text/xml, */*; q=0.01',
					    'Cache-Control': 'no-cache',
					    'X-Requested-With': 'XMLHttpRequest',
					    'Connection': 'keep-alive',
					    'Referer': 'http://www.send2u.com.br/sms/CadastroEventoLivre.xhtml',
					    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
					};

					var dataString = 'javax.faces.partial.ajax=true&javax.faces.source=j_idt189&javax.faces.partial.execute=%40all&javax.faces.partial.render=FormCadEvento&j_idt189=j_idt189&FormCadEvento=FormCadEvento&javax.faces.ViewState='+vs+'&j_idt122=0&j_idt124=&txtNome=&autoContatos_input=&autoContatos_hinput=&txtCel='+number+'&j_idt149=&txtInicio_input=&autoTemplate_input=&autoTemplate_hinput=&j_idt171='+text;

					var options = {
					    url: 'http://www.send2u.com.br/sms/CadastroEventoLivre.xhtml',
					    method: 'POST',
					    headers: headers,
					    body: dataString,
					    jar: jar
					};

					function callback(error, response, body) {
					    if (!error && response.statusCode == 200) {
					        // console.log(body);
					        fn()
					    }
					}

					request(options, callback);

		        }
		    }
		}

		request(options, callback);




    }
}

request(options, callback);


}




module.exports = function(str, n, fn){
	getLoginForm( function(err, form, args){
		var data = {};
		
		data.ViewState = form;

		generateCPF(function(cpf){
			data.cpf = cpf;
			generateEmail(function(err, mail){
				mail.result.str = mail.result.name+'%40developermail.com';
				data.email = mail.result;
				data.headers = args.headers;
				data.jar = args.jar;

				registerUser(data, function(currentJar, cookie){
					checkIds( data.email, function(err, ids){

						// console.log(ids, '???????')
						data.email.id = ids.result[0];

						getMessage( data.email, function(err, msg){
							// console.log('>>>>>>>>>>>>>>',msg)
							var cod = msg.result[0].value.match(/(C=C3=B3digo : \d(.*?){0,4})/g);
							var pass = msg.result[0].value.match(/(Usu=C3=A1rio: (.*?)<\/p>)/g);

							cod = cod[0].replace('C=C3=B3digo : ', '')
							pass = pass[0].replace('Usu=C3=A1rio: ', '')
							pass = pass.replace('</p>', '')
							

							login(cod, pass, function(jar,  head, vs){
								// i++;

									

								send(n, str, jar, head, vs, function(err){
									// console.log(line, 'ok');
								// callback('Ok');

									console.log(n, str )

									fn();

								})
							});
						} );
					} );
				})
			})

		});

	} );
}





