


var mongoose = require('mongoose'),
User = mongoose.model('User');
var Transfer = mongoose.model('Transfer');
var jwt = require("jsonwebtoken")
var request = require("request")
var moment = require("moment");

let u = null;

process.env.SECRET_KEY = 'secret'
    function numberToReal(numero) {
        var numero = numero.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return 'R$ '+numero.join(',');
    }

var crypto = require('../lib/Crypt');
var Notify = require('../lib/Notify');
var that = this;

exports.list_all_users = function(req, res) {
  User.find({}, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });

  console.log(crypto.decrypt('4ymJhA91xTkuQrdwTBB0Mg=='))
};




exports.fetchTransaction = function(req, res){
  console.log(req.params.id)
  Transfer.find({_id: req.params.id}).populate([{
    path: 'form',
    model: 'User'
  }]).exec( function(err, transfer){
    if( transfer.length > 0 ){
      if( !transfer[0].meta.accountNumber ){
          Transfer.find({_id:transfer[0]._id}).populate([{
            path: 'form',
            model: 'User'
          },{
            path: 'to',
            model: 'User'
          }]).exec( function(err, transfer){

         
            if( transfer.length > 0 ){
              res.status(200).json( transfer[0] );
            }
            return;
          })  
      }else{
         res.status(200).json( transfer[0] );
      }
    }
    console.log(transfer[0], err)

    return;
  })  
}
exports.userExists = function(field, value, fn){
  var query = {};

  query[field] = value

  User.find(query, function(err, users){

    console.log(err, users)
      if(err) return console.log(err);

      if (users.length == 0){
        fn(false);
        return;
        
      }

      fn(true);
  })
};



exports.create_a_user = function(req, res) {

  var user = req.body;




  if( user.password  ){
    user.password = crypto.encrypt( user.password+"" );
  }

    if( !user.fullname ){
      res.status(500).json({error: 'Insira um nome'});
      return;
    }


  that.userExists( 'email', user.email, function(exists){
   // console.log(exists, '><<<<<<<<<<<<')
    if( exists ){
      res.status(500).json({error: 'Usuário já existe'});
      return;
    }
    req.body.uname = req.body.uname.toLocaleLowerCase();
    var new_user = new User(req.body);
    new_user.save(function(err, user) {
      if (err){
        console.log(err);
      }


      res.json(user);
    });

  } ); 



};


exports.read_a_user = function(req, res) {
  User.findById(req.params.userId, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.getProfile = function(req, res){
  var token = req.headers['authorization'];
  var u = null;
  if( !token )
    res.status(401).send('Not authorized')

  var user  = jwt.verify( token, process.env.SECRET_KEY );

  if( !user )
    res.status(401).send('Not authorized')

  if( user.uname ){
    User.findOne({
            uname: user.uname        
    })
    .then(user => {
      var Myuser = JSON.stringify(user);
      Myuser = JSON.parse(Myuser)
      u = Myuser;

      var query = ['{"$where": "function() { ',
        'var deepIterate = function (obj, value) { ',
        '  for (var field in obj) { ',
        '    if (obj[field] == value){ return true; } ',
        '      var found = false; ',
        '      if ( typeof obj[field] === \'object\') { ',
        '        found = deepIterate(obj[field], value);',
        '        if (found) { ',
        '          return true; ',
        '        } ',
        '      } ',
        '    } ',
        '    return false; ',
        '  }; ',
        '  return deepIterate(this, \'' + user._id + '\'); }"',
        '}'].join('');


        if (user) {
          delete user.password;

          Transfer.find(JSON.parse(query)).then(extrat => {


          console.log('EXTRAT', extrat)
          // this gives an object with dates as keys
          const groups = extrat.reduce((groups, game) => {
            const date = moment(game.timex.split('T')[0]).format('DD/MM/Y');;
            if (!groups[date]) {
              groups[date] = [];
            }
            groups[date].push(game);
            return groups;
          }, {});

          // Edit: to add it in the array format instead
          const groupArrays = Object.keys(groups).map((date) => {
            return {
              date,
              transactions: groups[date]
            };
          });

          console.log('GROUPS', groupArrays)
        
            Myuser.extrat = groupArrays;
            res.json(Myuser)

          })
        } else {
            res.send('User does not exist')
        }
    })
  }

  // console.log('>>>>>>>>>', user)
};


exports.getAccount = function(req, res){
  // var token = req.headers['authorization'];
  // if( !token )
  //   res.status(401).send('Not authorized')

  // var user  = jwt.verify( token, process.env.SECRET_KEY );

  // if( !user )
  //   res.status(401).send('Not authorized')

  // if( user.uname ){
  //   User.findOne({
  //           uname: user.uname        
  //   })
  //   .then(user => {
  //       if (user) {
  //         delete user.password;
  //           res.json(user)
  //       } else {
  //           res.send('User does not exist')
  //       }
  //   })
  // }

  res.send('extrato')

  // console.log('>>>>>>>>>', user)
};
exports.validateTransfer = function( req, res){
  var token = req.headers['authorization'];
  var user  = jwt.verify( token, process.env.SECRET_KEY );

    User.findOne( {_id: user._id}, function(errUsr, Usr){


      function price_to_number(v){
          if(!v){return 0;}
          v=v.split('.').join('');
          v=v.split(',').join('.');
          return Number(v.replace(/[^0-9.]/g, ""));
      }

      if( !req.body.value ){
        res.status(500).json({error: 'Nenhum valor especificado!'});
        return;
      }

      if( !req.body.typet ){
        res.status(500).json({error: 'Selecione o tipo de transferência!'});
        return;
      }
      if( Usr.balance < req.body.value ){
        res.status(500).json({error: 'Saldo insuficiente!'});              
        return;
      }

      req.body.value = price_to_number( req.body.value ); 

      if( !user ){
        res.status(401).send('Not authorized')  
        return;
      }

      res.status(200).json({success: 'Transferência válida'})  



    });
}

exports.sendMoney = function(req, res){
  var token = req.headers['authorization'];
  var user  = jwt.verify( token, process.env.SECRET_KEY );

    User.findOne( {_id: user._id}, function(errUsr, Usr){
      if( Usr.smsCode != req.body.code){
        res.status(500).json({error: 'Código de verificação inválido, tente novamente!'});
        return;        
      }

      function price_to_number(v){
          if(!v){return 0;}
          v=v.split('.').join('');
          v=v.split(',').join('.');
          return Number(v.replace(/[^0-9.]/g, ""));
      }

      if( !req.body.value ){
        res.status(500).json({error: 'Nenhum valor especificado!'});
        return;
      }

      if( !req.body.typet ){
        res.status(500).json({error: 'Selecione o tipo de transferência!'});
        return;
      }
      if( Usr.balance < req.body.value ){
        res.status(500).json({error: 'Saldo insuficiente!'});              
        return;
      }

      req.body.value = price_to_number( req.body.value ); 

      if( !user )
        res.status(401).send('Not authorized')

       if( req.body.typet == 'TED' ){
        request.get('https://www.pagueveloz.com.br/api/v1/Bancos?codigo='+req.body.bankNumber, function(e, r) {
          var r = JSON.parse(r.body);
          var f = req.body;
          req.body['to'] = r.Sigla + ' - '+ req.body.agency + '/' +req.body.accountNumber; 
          req.body['form'] = user._id;
          req.body['meta'] = {

              accountNumber: req.body.accountNumber,
              bankNumber: req.body.bankNumber,
              agency: req.body.agency,
              cpf: req.body.cpf,
              bankInfo: r,
          };
          var b = req.body;
       

            that.minusBalance( user.uname, (Usr.balance - req.body.value), function(e, rs){
              if( e ){
                res.status(500).json({error: e});
              }            
              var Trans = new Transfer(req.body);

              Trans.save(function(err, tras){
                console.log('_++++++++++++',err, tras)
                res.status(200).json({caption: 'Transação aceita', res: tras});
                console.log('Você enviou '+numberToReal(req.body.value)+' para uma conta '+r.Nome)
                Notify({ 
                  to: Usr.pushId,
                  title: 'Transação OK!',
                  text: 'Você enviou '+numberToReal(req.body.value)+' para uma conta '+r.Nome,
                  style: 'none',
                  data: {
                    user_id: User._id,
                    type: 'money_sent',
                    meta: tras
                  }
                }, function(res){
                  console.log('sent!', res)
                }); 

              });   
            } )


             
        })
      }else{
        User.findOne( {uname: req.body.lessaId.toLocaleLowerCase()}, function(err, u){
          if( err || u == null  ){
            res.status(500).json({error: 'Usuário não encontrado!'});
            return;
          }

          if( Usr.balance < req.body.value ){
            res.status(500).json({error: 'Saldo insuficiente!'});              
            return;
          }
          that.addBalance( u.uname, u.balance + req.body.value, function(err, result){
            if( err ){
              res.status(500).json({error: err});
            }

                Notify({ 
                  to: u.pushId,
                  title: 'Transferência recebida',
                  text: 'Você acaba de receber '+numberToReal(req.body.value)+' de '+u.fullname+'!s',
                  style: 'none',
                  data: {
                    user_id: User._id,
                    type: 'money_received',
                    meta: {}  
                  }
                }, function(res){
                  console.log('sent!', res)
                  console.log( 'Você acaba de receber '+numberToReal(req.body.value)+' de '+u.fullname)
                });             
            // console.log((user.balance - req.body.value), user, Usr.balance, req.body.value)
            that.minusBalance( user.uname, (Usr.balance - req.body.value), function(e, rs){
              if( e ){
                res.status(500).json({error: e});
              }

              req.body['form'] = Usr._id;
              req.body['to'] = u._id+'';
              var Trans = new Transfer(req.body);

              Trans.save(function(err, tras){
                console.log('_++++++++++++',err, tras)
                res.status(200).json({caption: 'Transação aceita', res: tras});            
             Notify({ 
                  to: Usr.pushId,
                  title: 'Transação OK!',
                  text: 'Você enviou '+numberToReal(req.body.value)+' para '+u.fullname,
                  style: 'none',
                  data: {
                    user_id: User._id,
                    type: 'money_sent',
                    meta: tras
                  }
                }, function(res){
                  console.log('sent!', res)
                }); 
              });  

                // return here
            } )
          } );
          // console.log('Other', req.body, u)
          
        } )
      }

    } );
}

exports.addBalance = function(id, value, fn){

  User.findOneAndUpdate({uname: id}, {$set:{balance:value}}, {new: true}, (err, doc) => {
    if (err) {
        fn("Tente novamente mais tarde", null);
    }

    console.log(doc);
     fn(null, 'Transação aceita');
  });
}

exports.updatePushId = function(req, res, next){
  console.log('PUSH ID->', req.body.id)
  console.log('user ID->', req.params.id)
  User.findOneAndUpdate({_id: req.params.id}, {$set:{pushId:req.body.id}}, {new: true}, (err, doc) => {
    if (err) {
        res.status(500).end()
    }

    console.log(doc);
     res.status(200).send('id atualizado');
  });
}

exports.minusBalance = function(id, value, fn){

  User.findOneAndUpdate({uname: id}, {$set:{balance:value}}, {new: true}, (err, doc) => {
    if (err) {
        fn("Tente novamente mais tarde", null);
    }

    console.log(doc);
     fn(null, 'Transação aceita');
  });
}

exports.alterProfile = function(){

};
exports.login = function(req, res){
    var user = req.body;
    console.log(user, '<<<<<<<<<<<')
    if( !user.password ){
        res.status(500).json({error: 'Insira uma senha'});
        return;
    }
    if( !user.uname ){
        res.status(500).json({error: 'Insira o nome de usuário'});
        return;
    }

    User.find({uname: user.uname.toLocaleLowerCase()}, function(err, userReturned) {
      if (err){
        res.json(err);
        return;
      }

      if( userReturned.length == 0 ){
        res.status(500).json({error: 'Usuário não encontrado'});
        return;
      }


      User.find({uname: user.uname, password: crypto.encrypt(user.password)}, function(err, userReturnedP) {
        if (err){
          res.json(err);
          return;
          
        }

        if( userReturnedP.length == 0 ){
          res.status(401).json({error: 'Senha inválida'});
          return;
        }


    

        req.session.regenerate(function(err) {
            req.session.user = userReturned[0];
            req.session.user  =userReturned[0];
            req.user  =userReturned[0];
            req.session.save(function(err) {
                // session updated
                console.log("Session Before Redirect: ", req.session);
                if( user.type ){
                  if( user.type == 'REST' ){
                    
                    var token = jwt.sign(JSON.stringify(userReturned[0]), process.env.SECRET_KEY)
                       res.send(token);
                 
                    return;
                  }

                }else{
                      res.redirect('/?logged');
                      return;                  
                }
            });
        });


        // res.end();

        console.log(req.user)
        
      });

  });
}

exports.logout = function(req, res){
       req.session.regenerate(function(err) {
          req.session.user = null;
          req.session.user  =null;
          req.user  =null;
          req.session.save(function(err) {
              // session updated

            res.redirect('/');
            return;                  
      
          });
        });

}

exports.update_a_user = function(req, res) {
  console.log(req.params, req.body)
  User.findOne({ "_id": req.params.userId }, function(err, user) {
    if (err)
      res.send(err);


    console.log(err, user, '>>>', arguments)

    if( user.password != crypto.encrypt(req.body.password) ){
      req.body.password = crypto.encrypt(req.body.password)
    }
    
    User.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, user) {
      if (err)
        res.send(err);
      res.json(user);
    });
  });

};


exports.delete_a_user = function(req, res) {


  User.remove({
    _id: req.params.userId
  }, function(err, user) {
    if (err)
      res.send(err);
    res.json({ message: 'User successfully deleted' });
  });
};



