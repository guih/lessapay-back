'use strict';


var mongoose = require('mongoose'),
moment = require('moment');
var jwt = require("jsonwebtoken");

var User = mongoose.model('User');
var Transfer = mongoose.model('Transfer');



var SMS = require('../lib/Sms');

function numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
    return 'R$ '+numero.join(',');
}

var crypto = require('../lib/Crypt');
var that = this;

exports.editUser = function(req, res) {

  if( req.session.user && req.session.user.role[0] == 'adm' ){
    console.log({id: req.params.userId})
    User.findOne({_id: req.params.userId}, function(err, user){

      console.log('->', user)
        var numero = user.balance.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        user.balance = numero.join(',');
      res.render('edit-user', { user: req.session.user, userProfile: user,          title: 'LessaPay - Editar usuário',
          path: req.path,});
      return;
    })
  }else{
    res.render('index', {title: 'Login'})
    return;
  }



  // console.log(req.session, req.session.user)
};

exports.me = function(req, res) {

  if( req.session.user ){

    User.findOne({_id: req.session.user._id}, function(err, user){

      console.log('->', user)
        var numero = user.balance.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        user.balance = numero.join(',');
      res.render('me', { user: req.session.user, userProfile: user,          title: 'LessaPay - Meu Perfil',
          path: req.path,});
      return;
    })
  }else{
    res.render('me', {title: 'Meu perfil'})
    return;
  }



  // console.log(req.session, req.session.user)
};


exports.verify = function(req, res) {
  var token = req.headers['authorization'];
  var u = null;
  if( !token ){
    res.status(401).send('Not authorized')
    return;
  }

  var user  = jwt.verify( token, process.env.SECRET_KEY );
  var rand = function(){ return Math.floor(1000 + Math.random() * 999000); };

  var codeToken = rand(); 
  User.findOneAndUpdate({_id: user._id}, {smsCode: codeToken}, {new: true}, function(err, newUser) {
    if (err)
      res.staus(500).send(err);
    SMS( 
      'Seu codigo LessaPay e: '+codeToken, 
      user.phone.replace(/[^\d]/g,''), 
    function(){
      res.status(200).json({success: 'Confirme o número enviado via SMS para concluir a   transação.'});
    } );


  });
};

exports.verifyCode = function(req, res){
  var token = req.headers['authorization'];
  var u = null;
  if( !token )
    res.status(401).send('Not authorized')

  var user  = jwt.verify( token, process.env.SECRET_KEY );  




};


exports.settings = function(req, res) {

  if( req.session.user && req.session.user.role[0] == 'adm' ){
 
      res.render('settings', {          
        title: 'LessaPay - Dashboard',
        path: req.path, 
        user: req.session.user
      });
  }else{
    res.render('index', {title: 'Login'})
    return;
  }



  // console.log(req.session, req.session.user)
};



exports.register = function(req, res) {


      res.render('register', {          
        title: 'LessaPay - Registro',
        path: req.path, 
      });




  // console.log(req.session, req.session.user)
};

exports.editTransfer = function(req, res) {
        var QRCode = require('qrcode')

  Transfer.find({_id: req.params.id}).populate([{
    path: 'form',
    model: 'User'
  }]).exec( function(err, transfers){


    if( transfers ){
      if( !transfers[0].meta.accountNumber ){


         
        QRCode.toDataURL(transfers[0]._id+"", function (err, url) {

          // console.log('>>>>>>>>>>>>', url)
          Transfer.findOne({_id:transfers[0]._id}).populate([{
            path: 'form',
            model: 'User'
          },{
            path: 'to',
            model: 'User'
          }]).exec( function(err, transfer){
            var tr = transfer.toObject();

            tr.day = moment(tr.timex).format('DD/MM/YYYY');
            tr.hour = moment(tr.timex).format('HH:mm:ss');
            tr.timeHuman = moment(tr.timex).format('YYYY, MM DD\h');
            tr.QR = url;
            tr.value = numberToReal(tr.value);
            if( transfers.length > 0 ){
              res.render('receipt', { user: req.session.user, receipt: tr  ,        title: 'LessaPay - Comprovante',
          path: req.path,});
              // res.status(200).json( transfer[0] );
              return;
            }
          
          }) 
        })

 
      }else{
        QRCode.toDataURL(transfers[0]._id+"", function (err, url) {

          var tr = transfers[0].toObject();
            tr.day = moment(tr.timex).format('DD/MM/YYYY');
            tr.hour = moment(tr.timex).format('HH:mm:ss');
            tr.timeHuman = moment(tr.timex).format('YYYY, MM DD\h');
            tr.QR = url;
            tr.value = numberToReal(tr.value);


          res.render('receipt', { 
            user: req.session.user, 
            moment: moment, 
            receipt:tr,          title: 'LessaPay - Comprovante',
          path: req.path,
          });
          return;
        });
      }
    }else{

    }
    // console.log(transfers[0], err)

   
  })  


  // // if( req.session.user && req.session.user.role[0] == 'adm' ){
  //   console.log({id: req.params.id})
  //   Transfer.findOne({_id: req.params.id}, function(err, receipt){

  //     res.render('receipt', { user: req.session.user, receipt: receipt});
  //     return;
  //   })
  // }else{
  //   res.render('index', {title: 'Login'})
  //   return;
  // }



  // console.log(req.session, req.session.user)
};
exports.updateUsrAdmin = function(req, res) {

  if( req.session.user && req.session.user.role[0] == 'adm'){
    console.log({id: req.params.userId})
    console.log(req.body)
    var currentUser = req.body;



    User.findOne({_id: req.params.userId}, function(err, user){
        if( currentUser.password != '' ){
          currentUser.password = crypto.encrypt( currentUser.password );
        }else{
          currentUser.password = user.password;
        }

        User.findOneAndUpdate({_id: req.params.userId}, currentUser, {new: true}, function(err, newUser) {
          if (err)
            res.staus(500).send(err);
          

          res.status(200).json(newUser);
        });

      // // console.log('->', user)
      // var numero = user.balance.toFixed(2).split('.');
      // numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
      // user.balance = numero.join(',');
      // res.render('edit-user', { user: req.session.user, userProfile: user});
      // return;
    })
  }else{
    res.render('index', {title: 'Login'})
    return;
  }



  // console.log(req.session, req.session.user)
};

exports.dash = function(req, res) {
    function numberToReal(numero) {
      if(  ! numero.toFixed ) return numero;

        var numero = numero.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return 'R$ '+numero.join(',');
    }
    Array.prototype.sum = function (prop) {
        var total = 0
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i][prop]
        }
        return total
    }

    var currentUser = req.session.user;

    // console.log(req.session.user, '<<<<<<<<<<' )
  if( req.session.user){
    User.find({}).lean().exec( function(err, users){
      var f = users;
      // f = JSON.stringify( f );
      // f = JSON.parse(f);



      var total = users.reduce(function(prev, cur) {
        return prev + cur.balance;
      }, 0);        
       f = f.map( function(user){
        // user = user.toObject();
        user.balance = numberToReal( user.balance );
        return user;
      })
       var query = {};
       if(  currentUser.role[0] == 'adm' ){
        // console.log('>>>>>>>>>>>.',currentUser.balance)
        query = {};
       }else{
        query = {$or:[ {'form':currentUser._id}, {'to':currentUser._id} ]};

        currentUser.balance = numberToReal(currentUser.balance);       

       }

      Transfer.find(query).populate([{
        path: 'form',
        model: 'User'
      }]).lean().exec( function(err, transfers){

        if(  currentUser.role[0] == 'adm' ){
          // console.log('>>>>>>>>..',transfers)
          var ted = transfers.filter( function(t){

            return t.meta != undefined;
          })
          var intT = transfers.filter( function(t){
            return t.meta == undefined;
          });
        }else{
          var intT = 0, ted = 0;
        }

        transfers  = transfers.map(function(transfer){
          transfer.value = numberToReal( transfer.value  );
          transfer.timex = moment(transfer.timex).format('DD/MM/YYYY HH:mm:ss');
          return transfer;
        })
 

      
        res.render('dash', { 
          total: numberToReal(total), 
          user: req.session.user, 
          users: f,
          transfers: transfers,
          ted: ted.length,
          internal: intT.length,
          title: 'LessaPay - Dashboard',
          path: req.path,
        });
        return;
    })



    })
  }else{

  res.render('index', {title: 'Login'})
  return;
  }



  // console.log(req.session, req.session.user)
};
exports.home = function(req, res) {
  res.render('home',{title:''})
};

exports.bank = function(req, res) {
  function numberToReal(numero) {
      var numero = numero.toFixed(2).split('.');
      numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
      return 'R$ '+numero.join(',');
  }
  if( req.session.user){
    var query;
    if( req.session.user.role[0] == 'adm' ){
       query = {}
    }else{
      query =  {$or:[ {'form':req.session.user._id}, {'to':req.session.user._id} ]}
    }
    Transfer.find(query).populate([{
      path: 'form',
      model: 'User'
    }]).exec( function(err, transfers){

      var f = transfers;
      f = JSON.stringify( f );
      f = JSON.parse(f);



      var total = transfers.reduce(function(prev, cur) {
        return prev + cur.value;
      }, 0);        
       f = f.map( function(user){
        user.value =numberToReal( user.value );
        user.timex = moment(user.timex).format('DD/MM/YYYY HH:mm:ss');      
        return user;
      })

  
      transfers = transfers.map( function(user){
        // user = user.toObject();
        user.value =numberToReal( user.value );
        return user;
      })      
      // console.log(transfers, err)

      res.render('bank', { 
        user: req.session.user, 
        transfers: f,          
        title: 'LessaPay - Transações',
        path: req.path,
      });
      return;
    })
  }else{

    res.render('index', {title: 'Login'})
    return;
  }



  // console.log(req.session, req.session.user)
};


exports.colectorExists = function(field, value, fn){
  var query = {};

  query[field] = value

  User.find(query, function(err, colectors){

    console.log(err, colectors)
      if(err) return console.log(err);

      if (colectors.length == 0){
        fn(false);
        return;
        
      }

      fn(true);
  })
};



exports.create_a_colector = function(req, res) {

  var colector = req.body;

  if( colector.password  ){
    colector.password = crypto.encrypt( colector.password );
  }


   that.colectorExists( 'email', colector.email, function(exists){
   console.log(exists, '><<<<<<<<<<<<')
    if( exists ){
      res.status(500).json({error: 'Usuário já existe'});
      return;
    }
      var new_colector = new User(req.body);
      new_colector.save(function(err, colector) {
        if (err){
          console.log(err);
        }


        res.json(colector);
      });

   } ); 



};


exports.read_a_colector = function(req, res) {
  User.findById(req.params.colectorId, function(err, colector) {
    if (err)
      res.send(err);
    res.json(colector);
  });
};


exports.update_a_colector = function(req, res) {
  User.findOneAndUpdate({_id: req.params.colectorId}, req.body, {new: true}, function(err, colector) {
    if (err)
      res.send(err);
    res.json(colector);
  });
};


exports.delete_a_colector = function(req, res) {


  User.remove({
    _id: req.params.colectorId
  }, function(err, colector) {
    if (err)
      res.send(err);
    res.json({ message: 'User successfully deleted' });
  });
};



