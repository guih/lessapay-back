jQuery(document).ready(function(){
  window.LessaPayFront = {};

  LessaPayFront.init = function(){
    feather.replace() 
    $('#login-form').find('input').on('keyup', function(){
      $('.alert').hide();
    })   
  }

  LessaPayFront.login = function(){
    $.ajax({ 
      url: '/app', 
      method: 'POST', 
      data: $('#login-form').serialize(), 
      success: function(res){ 
        $('.alert').removeClass('alert-danger').addClass('alert-success').text('Autenticado com sucesso!').fadeIn(); 
        
        setTimeout(function(){
          window.location.href=window.location.href
        },1000)
      },
      error: function(e){ 
        console.log(e) 
        $('.alert').addClass('alert-danger').text(e.responseJSON.error).fadeIn(); 

      }

    })    
  }

  LessaPayFront.register = function(){
    $.ajax({ 
      url: '/users', 
      method: 'POST', 
      data: $('#register-form').serialize(), 
      success: function(res){ 
        $('.alert').removeClass('alert-danger').addClass('alert-success').text('Cadastrado com sucesso!').fadeIn(); 
        
        setTimeout(function(){
          window.location.href='/app'
        },1000)
      },
      error: function(e){ 
        console.log(e) 
        $('.alert').addClass('alert-danger').text(e.responseJSON.error).fadeIn(); 

      }

    })    
  }

  window.onload = function(){
    LessaPayFront.init();
  }
});