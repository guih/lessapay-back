jQuery(document).ready(function(){
  window.LessaPay = {};

  LessaPay.init = function(){
    feather.replace()
    $('table').DataTable();    
    $('[selected=false]').removeAttr('selected')
    $('[selected=true]').attr('selected', 'selected')
    
  }

  LessaPay.editUser = function(){
    $.ajax({
      type: "POST",
      url: $('#edit-user').data('endpoint'),
      data: $('#edit-user').serialize(),
      success: function(){ 
        $('.message').addClass('alert-success').text('Usuário atualizado!').fadeIn();
        setTimeout(function(){
          window.location.href = window.location.href;          
        },2000)
      },
      error: function(){ $('.message').addClass('alert-danger').text('Erro ao atualizar usuário').fadeIn() }
    });
  }
  window.onload = function(){
    LessaPay.init();
  }
});