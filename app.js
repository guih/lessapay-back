var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var session = require('express-session')
var cors = require('cors')

var MongoStore = require('connect-mongo')(session);



var userAPI = require('./routes/index');
var collectorAPI = require('./routes/collectors');


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://adm:37590538@localhost/Tododb?authSource=admin'); 
// mongoose.connect('mongodb://localhost/Tododb'); 

var db = mongoose.connection;

var Task = require('./models/User')
var Collectors = require('./models/Collector')
var Transfer = require('./models/Transfer');

var app = express();
app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(session({
    resave: false,
    secret: '121321321321',
    store: new MongoStore({ mongooseConnection: db }),
    cookie: {
        secure: false, // Secure is Recommeneded, However it requires an HTTPS enabled website (SSL Certificate)
        maxAge: 864000000 // 10 Days in miliseconds
    }
}));
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

// if (app.get('env') === 'production') {
//   app.set('trust proxy', 1) // trust first proxy
//   sess.cookie.secure = true // serve secure cookies
// }
 

userAPI(app);
collectorAPI(app);
// app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
