'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/UserController');

  // todoList Routes
  app.route('/users')
    .get(todoList.list_all_users)
    .post(todoList.create_a_user);

  app.route('/logout')
    .get(todoList.logout)


   app.route('/profile')
    .get(todoList.getProfile)
    .post(todoList.alterProfile)

   app.route('/transfer')
    .get(todoList.getAccount)
    .post(todoList.sendMoney)  

   app.route('/validateTransfer')
    .post(todoList.validateTransfer);      


   app.route('/transaction/:id')
    .post(todoList.fetchTransaction);

   app.route('/pushId/:id')
    .post(todoList.updatePushId);    


  app.route('/users/:userId')
    .get(todoList.read_a_user)
    .put(todoList.update_a_user)
    .delete(todoList.delete_a_user);
};
