'use strict';
module.exports = function(app) {
  var colector = require('../controllers/ColectorController');
  var user = require('../controllers/UserController');

  // colector Routes
  app.route('/')
    .get(colector.home);

  app.route('/verify')
    .get(colector.verify)    
    .get(colector.verifyCode);    
  // colector Routes
  app.route('/app')
    .get(colector.dash)
    .post(user.login);


  app.route('/me')
    .get(colector.me)

  app.route('/register')
    .get(colector.register)
    .post(user.login);    

  app.route('/bank')
    .get(colector.bank)
    .post(user.login); 

  app.route('/edit/user/:userId')
    .get(colector.editUser)
    .post(colector.updateUsrAdmin);  

  app.route('/settings')
    .get(colector.settings)
    .post(colector.updateUsrAdmin);       

  app.route('/edit/transfer/:id')
    .get(colector.editTransfer)
    .post(colector.updateUsrAdmin);         




  app.route('/colectors/:colectorId')
    .get(colector.read_a_colector)
    .put(colector.update_a_colector)
    .delete(colector.delete_a_colector);
};
